# ~/.bashrc: executed by bash(1) for non-login shells.
# see /usr/share/doc/bash/examples/startup-files (in the package bash-doc)
# for examples

# If not running interactively, don't do anything
case $- in
    *i*) ;;
      *) return;;
esac

# don't put duplicate lines or lines starting with space in the history.
# See bash(1) for more options
HISTCONTROL=ignoreboth

# append to the history file, don't overwrite it
shopt -s histappend

# for setting history length see HISTSIZE and HISTFILESIZE in bash(1)
HISTSIZE=1000
HISTFILESIZE=2000

# check the window size after each command and, if necessary,
# update the values of LINES and COLUMNS.
shopt -s checkwinsize

# If set, the pattern "**" used in a pathname expansion context will
# match all files and zero or more directories and subdirectories.
#shopt -s globstar

# make less more friendly for non-text input files, see lesspipe(1)
[ -x /usr/bin/lesspipe ] && eval "$(SHELL=/bin/sh lesspipe)"

# set variable identifying the chroot you work in (used in the prompt below)
if [ -z "${debian_chroot:-}" ] && [ -r /etc/debian_chroot ]; then
    debian_chroot=$(cat /etc/debian_chroot)
fi

# set a fancy prompt (non-color, unless we know we "want" color)
case "$TERM" in
    xterm-color|*-256color) color_prompt=yes;;
esac

# uncomment for a colored prompt, if the terminal has the capability; turned
# off by default to not distract the user: the focus in a terminal window
# should be on the output of commands, not on the prompt
#force_color_prompt=yes

if [ -n "$force_color_prompt" ]; then
    if [ -x /usr/bin/tput ] && tput setaf 1 >&/dev/null; then
	# We have color support; assume it's compliant with Ecma-48
	# (ISO/IEC-6429). (Lack of such support is extremely rare, and such
	# a case would tend to support setf rather than setaf.)
	color_prompt=yes
    else
	color_prompt=
    fi
fi

if [ "$color_prompt" = yes ]; then
    PS1='${debian_chroot:+($debian_chroot)}\[\033[01;32m\]\u@\h\[\033[00m\]:\[\033[01;34m\]\w\[\033[00m\]\$ '
else
    PS1='${debian_chroot:+($debian_chroot)}\u@\h:\w\$ '
fi
unset color_prompt force_color_prompt

# If this is an xterm set the title to user@host:dir
case "$TERM" in
xterm*|rxvt*)
    PS1="\[\e]0;${debian_chroot:+($debian_chroot)}\u@\h: \w\a\]$PS1"
    ;;
*)
    ;;
esac

# enable color support of ls and also add handy aliases
if [ -x /usr/bin/dircolors ]; then
    test -r ~/.dircolors && eval "$(dircolors -b ~/.dircolors)" || eval "$(dircolors -b)"
    alias ls='ls --color=auto'
    #alias dir='dir --color=auto'
    #alias vdir='vdir --color=auto'

    alias grep='grep --color=auto'
    alias fgrep='fgrep --color=auto'
    alias egrep='egrep --color=auto'
fi

# colored GCC warnings and errors
#export GCC_COLORS='error=01;31:warning=01;35:note=01;36:caret=01;32:locus=01:quote=01'

# some more ls aliases
alias ll='ls -alF'
alias la='ls -A'
alias l='ls -CF'

# Add an "alert" alias for long running commands.  Use like so:
#   sleep 10; alert
alias alert='notify-send --urgency=low -i "$([ $? = 0 ] && echo terminal || echo error)" "$(history|tail -n1|sed -e '\''s/^\s*[0-9]\+\s*//;s/[;&|]\s*alert$//'\'')"'

# Alias definitions.
# You may want to put all your additions into a separate file like
# ~/.bash_aliases, instead of adding them here directly.
# See /usr/share/doc/bash-doc/examples in the bash-doc package.

if [ -f ~/.bash_aliases ]; then
    . ~/.bash_aliases
fi

# enable programmable completion features (you don't need to enable
# this, if it's already enabled in /etc/bash.bashrc and /etc/profile
# sources /etc/bash.bashrc).
if ! shopt -oq posix; then
  if [ -f /usr/share/bash-completion/bash_completion ]; then
    . /usr/share/bash-completion/bash_completion
  elif [ -f /etc/bash_completion ]; then
    . /etc/bash_completion
  fi
fi
#=======================================================================================================================
#=======================================================================================================================
# MY EDITS:
#
#----------------------------------------------------------------------
# Notes:
#---------
#
# 1) To make this config work in arch or fedora swap apt with pacman or dnf respectively.
#
# 2) REMEMBER! Linux iS cAsE SensiTive. Pay attention when using the terminal.
#
#----------------------------------------------------------------------
#
# CONTENTS:
#
# 1. System configs and aliases.
#
# 2. Block device manipulation (mount/unmount/format drives USE WITH CAUTION!!).
#
# 3. System navigation & general terminal usage.
#
# 4. File maniplulation/conversion.
#
# 5. Applications & Media.
#
#
#
#============================================
#
# 1) SYSTEM CONFIGS & ALIASES:
#------------------------------

# VI mode: Allow VIm keybindings in bash:

set -o vi



# Aliases:

alias api="sudo apt install"
alias snapi="sudo snap install"
alias apu="sudo apt update"
alias apg="sudo apt upgrade"
alias snow="shutdown now"

# Battery status in the terminal. Usefull if you live in the tty.
#alias bat=" upower -i $(upower -e | grep BAT) | grep --color=never -E "state|to\ full|to\ empty|percentage""


#Set screen Brightness with xrandr:

alias br1="xrandr --output LVDS-1 --brightness 1" # Brightness @ 100%
alias br5="xrandr --output LVDS-1 --brightness 0.5" # Brightness @ 50%
alias br7="xrandr --output LVDS-1 --brightness 0.7" # Brightness @ 70%
alias br2="xrandr --output LVDS-1 --brightness 0.25" # Brightness @ 25%


# Set multi-screen layouts (requires scripts from 'myconfs/layouts' to work.

alias desk="/home/adnanbaig/.myconfs/layouts/desk.sh"
alias dual="/home/adnanbaig/.myconfs/layouts/dual.sh"
alias lap="/home/adnanbaig/.myconfs/layouts/laptop.sh"





#============================================
#
# 2) Block device manipulation (mount/unmount/format drives USE WITH CAUTION!!).
# #-------------------------------------------------------------------------------


#Flash USB (mkfs):

alias mk4="sudo mkfs.ext4"

alias mk4a="sudo mkfs.ext4 /dev/sda"
alias mk4b="sudo mkfs.ext4 /dev/sdb"
alias mk4c="sudo mkfs.ext4 /dev/sdc"
alias mk4d="sudo mkfs.ext4 /dev/sdd"
alias mk4e="sudo mkfs.ext4 /dev/sde"



# Mounting/unmounting:

alias mount="sudo mount"
alias umount="sudo umount"

alias mounta="sudo mount /dev/sda"
alias mountb="sudo mount /dev/sdb"
alias mountc="sudo mount /dev/sdc"
alias mountd="sudo mount /dev/sdd"
alias mounte="sudo mount /dev/sde"

alias umounta="sudo umount /dev/sda"
alias umountb="sudo umount /dev/sdb"
alias umountc="sudo umount /dev/sdc"
alias umountd="sudo umount /dev/sdd"
alias umounte="sudo umount /dev/sde"


# call upon unetbootin or balena etcher. I'll swap this out for dd commands once I've mastered them.

# alias unet
# alias balena

#============================================
#
# 3) System navigation & general terminal usage.
# #-------------------------------------------------------------------------------


# Go home:

alias cdh="cd ~" # cdh=home
alias home="cd /home/adnanbaig" # home = your_home_folder

# alias home="cd /home/firstnamelastname" # mh = your_home_folder 


# Clearing the screen and listing files with one command:

alias c="clear"
alias cl="clear && ls"
alias cla="clear && ls -alth"

# List files:

alias lsa="ls -alth"




#============================================
#
# 4. File maniplulation/conversion.
# #-------------------------------------------------------------------------------



# Edit configuration files:

alias eb="vim ~/.bashrc" # edit .bashrc
alias ev="sudo vim /usr/share/vim/vimrc" # edit .vimrc
alias ei="vim ~/.config/i3/config" # edit your i3wm config file.
alias ep="vim ~/.profile" # edit .profile


# Source configuration files:

alias seb="source /home/adnanbaig/.bashrc" # source .bashrc
alias sep="source /home/adnanbaig/.profile" # source .profile

# need to add ~/.vimrc to this asap!


# make files executable:
alias mod="chmod u+x" # run "mod filename" to make executable"

# convert images from .ppm to .png/.jpg (usefull when taking screenshots from the termial/tty):
alias mogp="~/.myScripts/png.sh"
alias mogj="~/.myScripts/jpg.sh"


# Combine PDF files together. Usefull when using TOC's but really you should just learn to love LaTex!

combine-pdf() { gs -dBATCH -dNOPAUSE -sDEVICE=pdfwrite -sOutputFile=combinedpdf_`date +'%F_%Hh%M'`.pdf -f "$@" ;}


#============================================
#
# 5. Applications & Media.
# #-------------------------------------------------------------------------------


# emacs:

# Start emacs in the terminal:

alias tmacs="emacs -nw"



# Youtube-DL download as .mp3. run "youtube-dl url" to download as .mp4


alias ytd="youtube-dl"
alias yta="youtube-dl --verbose -x --audio-format mp3"







#=======================================================================================================================
#=======================================================================================================================

############===========END_OF_MY_EDITS___[for now]___==================##################################

#=======================================================================================================================
#=======================================================================================================================
